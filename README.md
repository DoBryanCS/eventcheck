# eventcheck

EVENT CHECK
Fait par Do Khoa / Hangi Kisoni Eusèbe

VISION GLOBALE DU PROJET: <br/>
Bienvenue dans notre application web de gestion d'événements, l'outil parfait pour les organisateurs d'événements qui souhaitent planifier une expérience exceptionnelle pour leurs invités. Que vous organisiez une conférence, une soirée scolaire ou une réunion d'affaires, notre application est conçue pour vous aider à gérer efficacement tous les aspects de votre événement. Avec notre application, vous pouvez facilement importer votre liste d'invités et les informations relatives à votre événement. Notre système intelligent de reconnaissance facial vous permettra d'identifier rapidement chaque invité, de suivre les confirmations de présence. Notre application vous permet également de personnaliser les invitations en fonction des préférences pour chaque invité. Vous pouvez facilement ajouter des détails sur les horaires, les lieux, les activités, etc. Avec notre application, vous pouvez également gérer les listes d'invités et suivre les présences pour chaque événement. Notre application est facile à utiliser, intuitive et fiable. Nous travaillons sans relâche pour améliorer constamment notre produit et nous sommes toujours prêts à offrir un excellent support client pour répondre à toutes vos questions et besoins. Alors n'hésitez plus, essayez notre application web de gestion d'événements dès maintenant et offrez à vos invités une expérience inoubliable! <br/>

---------------------------------------------------------------------------------------------------------------------------------

PRÉREQUIS: <br/>
1-Installer Python <br/>
2-Installer Node.js <br/>
3-Installer Visual Studio Community avec C++ Compiler installé <br/>
4-Installer CMake et l'ajouter dans les variables denvironnement de tous les utilisateurs <br/>

INSTALLATION BACKEND: <br/>
1-Installer virtualenv pour creer un environnemnt virtuel: pip install virtualenv <br/>
2-Créer lenvironnement virtuel: virtualenv venv <br/>
3-Activer l'environnement virtuel: ./venv/Scripts/activate <br/>
4-Installer les librairies: <br/>
    4.1-pip install cmake <br/>
    4.2-pip install face_recognition opencv-python flask flask-cors firebase-admin <br/>
5-Runner le backend: python main.py <br/>

INSTALLATION FRONTEND: <br/>
1-Installer les dépendances: npm install <br/>
2-Runner le frontend: npm start  <br/>

---------------------------------------------------------------------------------------------------------------------------------

OUTILS UTILISÉS: <br/>
-Plateforme de développement Firebase (Cloud Firestore et Firebase storage) <br/> 
-Plateforme de développement Gitlab <br/>
-Outil de gestion et d'organisation Notion <br/>
-API face_recognition  <br/>
-Librairie OpenCv <br/>
-SDK firebase-admin <br/>
-Langage de programmation Javascript et Python <br/>
-Framework Flask <br/>
-Framework React <br/>
-Framework Bulma <br/>